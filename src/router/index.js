import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Home, Jadwal, Akun, jadwal } from '../pages';
import { BottomTabNavigator } from '../components';


const Tab = createBottomTabNavigator();
const Stack = createNativeStackNavigator();

const MainApp = () =>{
  return (
    <Tab.Navigator tabBar={props => <BottomTabNavigator {...props} />}>
        <Tab.Screen name="Home" component={Home} options={{headerShown: false}}/>
        <Tab.Screen name="jadwal" component={jadwal} options={{headerShown: false}}/>
        <Tab.Screen name="Akun" component={Akun} options={{headerShown: false}}/>
      </Tab.Navigator>
  )
}

const Router = () => {
  return (
    <Stack.Navigator>
    <Stack.Screen name="MainApp" component={MainApp}  options={{headerShown: false}} />
    </Stack.Navigator>
  )
}

export default Router

const styles = StyleSheet.create({})