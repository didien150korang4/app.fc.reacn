import {StyleSheet, Text, View, Image, Dimensions} from 'react-native';
import React, {Component} from 'react';
import HeaderInformation from '../../components/HeaderInformation';
import {ImageBanner} from '../../assets';
import Layanan from '../../components/Layanan';
import {BoxKlub, SmallButton} from '../../components';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Layanan: 'Booking',
    };
  }

  clickLayanan(value) {
    this.setState({
      Layanan: value,
    });
  }

  render() {
    return (
      <View style={styles.page}>
        <View style={styles.wrapperHeader}>
          <HeaderInformation />
          <Image source={ImageBanner} style={styles.banner} />
        </View>
        <View style={styles.wrapperLabel}>
          <Text style={styles.label}>
            Ayo!! <Text style={styles.textBold}> Futsall Squat </Text>
          </Text>
          <View style={styles.Layanan}>
            <Layanan
              title="Paket Langganan"
              onePress={() => this.clickLayanan('paket')}
              active={this.state.Layanan === 'paket' ? true : false}
            />
            <Layanan
              title="Booking Lapangan"
              onePress={() => this.clickLayanan('booking')}
              active={this.state.Layanan === 'booking' ? true : false}
            />
            <Layanan
              title="Join Team"
              onePress={() => this.clickLayanan('join')}
              active={this.state.Layanan === 'join' ? true : false}
            />
          </View>
          <View style={styles.labelSparing}>
            <Text style={styles.label}>
              Pilih Team<Text style={styles.textBold}> Sparing</Text> Kamu
            </Text>
            <SmallButton text="Lihat Semua" />
          </View>
        </View>
        <View style={styles.klub}>
          <BoxKlub klub="11 FC" statistik="WWWDDDD" />
        </View>
      </View>
    )
  }
}

export default Home;

// const windwHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#ffffff',
    color: '#000000',
  },
  wrapperHeader: {
    paddingHorizontal: 3,
    paddingTop: 30,
  },
  banner: {
    marginLeft: 29,
    marginTop: 15,
  },
  label: {
    fontSize: 18,
    color: '#000000',
    fontFamily: 'poppins-Light',
  },
  textBold: {
    fontSize: 14,
    color: '#000000',
    fontFamily: 'poppins-Bold',
  },
  wrapperLabel: {
    paddingHorizontal: 30,
    marginTop: 15,
  },
  Layanan: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 35,
  },
  labelSparing: {
    flexDirection: 'row',
    marginTop: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
klub :{
  flexDirection:'row'
}
});
