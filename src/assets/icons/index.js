import IconHome from "../icons/home.svg";
import IconhomeAktif from "../icons/homeAktiF.svg";
import Iconjadwal from "../icons/jadwal.svg";
import IconjadwalAktif from "../icons/jadwalAktiF.svg";
import Iconuser from "../icons/user.svg";
import IconuserAktif from "../icons/userAktiF.svg"
import IcondefaultUser from "../icons/defaultUser";
import Iconstadion from "../icons/stadion.svg";
import IconBooking from "../icons/booking.svg";
import IconLangganan from '../icons/langganan.svg';
import IconjoinTeam from '../icons/joinTeam.svg';


export {
    IconHome,
    IconhomeAktif,
    Iconjadwal,
    IconjadwalAktif,
    Iconuser,
    IconuserAktif,
    IcondefaultUser,
    Iconstadion,
    IconBooking,
    IconLangganan,
    IconjoinTeam
};