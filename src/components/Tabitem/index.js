import { StyleSheet, Text, TouchableOpacity } from 'react-native'
import React from 'react'
import { WARNA_SEKUNDER, WARNA_UTAMA } from '../../utils/constans'
import {IconHome, IconhomeAktif, Iconjadwal, IconjadwalAktif, Iconuser, IconuserAktif} from '../../assets'

const TabItem = ({label, isFocused, onLongPress, onPress}) => {
    const Icon = () => {
        if(label=== "Home"){
            return isFocused ? <IconhomeAktif/> : <IconHome/>;
        }
        if(label=== "jadwal"){
            return isFocused ? <IconjadwalAktif/> : <Iconjadwal/>;
        }
        if(label=== "Akun"){
            return isFocused ? <IconuserAktif/> : <Iconuser/>;
        }
    }
  return (
    <TouchableOpacity
    onPress={onPress}
    onLongPress={onLongPress}
    style={isFocused ? styles.containerFocus: styles.container}
  > 
  <Icon />
    {isFocused &&   <Text style = {styles.text}>
    {label.toUpperCase()}
</Text>}
  </TouchableOpacity>
  )
}
export default TabItem

const styles = StyleSheet.create({
  container: {
    alignContent: 'center',
    padding:5,
    borderTopLeftRadius:15,
    borderTopRightRadius:15
  },
  containerFocus :{
    alignContent: 'center',
    padding:5,
    backgroundColor: WARNA_SEKUNDER,
    flexDirection :'row',
    borderRadius : 10,
  },
  text : {
    color:WARNA_UTAMA,
    fontSize:20,
    marginLeft:8,
    fontFamily:'poppins-Bold'
  },
})