import {StyleSheet, Text, View} from 'react-native';
import React from 'react';
import {IcondefaultUser, IconjadwalAktif, Iconstadion} from '../../assets';

const HeaderInformation = () => {
  return (
    <View style={styles.container}>
      <View style={styles.img}>
        <IcondefaultUser />
      </View>
      <View style={{flex:2}}>
        <Text
          style={styles.hello}>
          Hello
        </Text>
        <Text
          style={{
            fontSize: 20,
            paddingHorizontal: 10,
            paddingTop: 2,
            color: '#000000',
            fontSize: 20,
            fontFamily: 'arialbold',
          }}>
          Alfiand!
        </Text>
      </View>
      <View>
        <Text
          style={styles.jadwalku}>
          19:00 Wib
        </Text>
        <Text
          style={styles.jam}>
          Sabtu 08-05-2023
        </Text>
      </View>
      <View>
      <Iconstadion />
      <Text style={{fontSize: 12, color: '#000000'}}>Next jadwal</Text>
      </View>
      <View />
    </View>
  );
};

export default HeaderInformation;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingTop: 10,
    // justifyContent:'space-between'
  },
  img: { 
    width:50,
  },
  hello:{
    fontSize: 20,
            paddingHorizontal: 10,
            paddingTop: 5,
            color: '#000000',
            fontSize: 15,
  },
  jam:{
      fontSize: 12,
      paddingHorizontal: 5,
      paddingTop: 2,
      color: '#000000',
  },
  jadwalku:{
      fontSize: 20,
      paddingHorizontal: 5,
      paddingTop: 5,
      color: '#000000',
      fontSize: 10,
  }
});
