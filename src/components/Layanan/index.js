import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {IconLangganan, IconBooking, IconjoinTeam} from '../../assets';
import {WARNA_SEKUNDER, WARNA_UTAMA} from '../../utils/constans';

const Layanan = ({title, active, onePress}) => {
  const Icon = () => {
    if (title === 'Paket Langganan') return <IconLangganan />;
    if (title === 'Booking Lapangan') return <IconBooking />;
    if (title === 'Join Team') return <IconjoinTeam />;
    return <IconjoinTeam />;
  };
  return (
    <TouchableOpacity style={styles.container(active)} onPress={onePress}>
      <View style={styles.Icon}>
        <Icon />
        <Text style={styles.text(active)}>{title.replace(' ', '\n')}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default Layanan;

const styles = StyleSheet.create({
  container: active => ({
    alignItems: 'center',
    backgroundColor: active ? WARNA_UTAMA : '#FFFFFF',
    borderRadius: 10,
    padding: 8,
    borderColor: WARNA_SEKUNDER,
    borderWidth: active ? 0 : 2,
  }),

  text: active =>({
    fontSize: 15,
    fontFamily: 'poppins-Bold',
    color: active ? 'width' : WARNA_SEKUNDER,
    textAlign: 'center',
  }),
  Icon: {
    marginTop: -30,
  },
});
