import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import { WARNA_UTAMA } from '../../utils/constans';

const SmallButton = ({text}) => {
  return (
    <TouchableOpacity style={styles.container}>
      <Text style={styles.text}>({text})</Text>
    </TouchableOpacity>
  );
};

export default SmallButton;

const styles = StyleSheet.create({
    container:{
       backgroundColor:WARNA_UTAMA,
       padding:3,
       borderRadius:3,
       fontFamily: 'poppins-Bold'
    },
    text :{
        color:'#ffffff',
        fontSize:10,
        fontFamily: 'poppins'
    }
});
